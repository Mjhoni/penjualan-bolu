<?php 
    require_once("koneksi.php");

    $id_bolu = $_GET['id_bolu'];

    $sql_cari = "SELECT * FROM bolux WHERE id_bolu ='$id_bolu'";
    $query = mysqli_query($koneksi, $sql_cari);
    $result = mysqli_fetch_assoc($query);

    if(isset($_POST['submit'])){
        $id_bolu = $_POST['id_bolu'];
        $nama_bolu = $_POST['nama_bolu'];
        $stok = $_POST['stok'];
        $harga_beli = $_POST['harga_beli'];
        $harga_jual= $_POST['harga_jual'];
        $nama_supplier = $_POST['nama_supplier'];
    
        $sql_edit = "UPDATE bolux SET nama_bolu = '$nama_bolu', stok = '$stok', harga_beli = '$harga_beli', harga_jual = '$harga_jual', nama_supplier = '$nama_supplier' WHERE id_bolu= '$id_bolu' ";
        mysqli_query($koneksi, $sql_edit);
    
        header("Location:index.php");
    }

?>

<!DOCTYPE html>
<html>
<head>
	<title>Responsi Basis Data</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="judul">		
		<h1>Bolux Jogja</h1>
		<h2>Edit Data Bolu</h2>
	</div>
	
	<br/>
 
	<a href="index.php">Lihat Semua Data</a>
 
	<br/>
	<h3>Silahkan Edit Data bolu:</h3>
	<form action="edit.php" method="POST">		
		<table>
            <tr>
				<td>Id bolu</td>
				<td><input type="number" name="id_bolu" value="<?= $result['id_bolu']; ?>"></td>					
			</tr>
			<tr>
				<td>Nama bolu</td>
				<td><input type="text" name="nama_bolu" value="<?= $result['nama_bolu']; ?>"></td>					
			</tr>	
			<tr>
				<td>Stok</td>
				<td><input type="number" name="stok" value="<?= $result['stok']; ?>"></td>					
			</tr>	
			<tr>
				<td>Harga Beli</td>
				<td><input type="number" name="harga_beli" value="<?= $result['harga_beli']; ?>"></td>					
			</tr>	
            <tr>
				<td>Harga Jual</td>
				<td><input type="number" name="harga_jual" value="<?= $result['harga_jual']; ?>"></td>					
			</tr>
            <tr>
				<td>Nama Supplier</td>
				<td><input type="text" name="nama_supplier" value="<?= $result['nama_supplier']; ?>"></td>					
			</tr>				
		</table>
        <button name="submit" type="submit">Ubah Data</button>
	</form>
</body>
</html>