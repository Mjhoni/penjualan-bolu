<?php

    require_once("koneksi.php");
    $sql_get = "SELECT * FROM bolux";
    $query_pj = mysqli_query($koneksi, $sql_get);
    $results = [];
    while($row = mysqli_fetch_assoc($query_pj)){
        $results[] = $row;
    }

?>

<!DOCTYPE html>
<html>
<head>
	<title>Responsi Basis Data</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<style>
		a:link, a:visited {
  		background-color: #f44336;
  		color: white;
  		padding: 5px 3px;
  		text-align: center;
  		text-decoration: none;
  		display: inline-block;
		}

		a:hover, a:active {
  		background-color: red;
		}
		th{
		padding: 15px 3px;
		background-color: #f44336;
		text-align: center;
  		text-decoration: none;
 		}
	</style>
</head>
<body>
<center>
	<div class="judul">		
		 <img src="bolu.jpg" alt="Gambar Bolu" width="300" height="100" style="border-radius: 50%;">
		<h1>Bolux Jogja</h1>
		<h2>Jalan Keganangan No. 117, Banguntapan, Bantul, Yogyakarta</h2>
	</div>
	<br/>
 
	<br/>

	<table border="1" class="table">
		<tr>	
            <th>Id_bolu</th>
			<th>Nama bolu</th>
			<th>Stok</th>
			<th>Harga Beli</th>
			<th>Harga Jual</th>	
            <th>Nama Supplier</th>
            <th>Opsi</th>		
		</tr>
        
        <?php
            foreach($results as $result) :
        ?>
		<tr>
			
            <td><?php echo $result['id_bolu']; ?></td>
			<td><?php echo $result['nama_bolu']; ?></td>
			<td><?php echo $result['stok']; ?></td>
			<td><?php echo $result['harga_beli']; ?></td>
            <td><?php echo $result['harga_jual']; ?></td>
            <td><?php echo $result['nama_supplier']; ?></td>
			<td>
				<a href="edit.php?id_bolu=<?=$result['id_bolu'];?>">Edit</a> ||
				<a href="hapus.php?id_bolu=<?=$result['id_bolu'];?>">Hapus</a>					
			</td>
		</tr>
		
        <?php
            endforeach;
        ?>
	</table>
    <br><br><a  href="input.php">+ Tambah Data Baru</a>
	<br><br><a  href="penjualan.php">+ Lihat Data Penjualan</a>
    </center>
</body>
</html>
