<?php

    require_once("koneksi.php");
    $sql_gett = "SELECT * FROM penjualan";
    $query_pjn = mysqli_query($koneksi, $sql_gett);
    $resultss = [];
    while($rows = mysqli_fetch_assoc($query_pjn)){
        $resultss[] = $rows;
    }

?>

<!DOCTYPE html>
<html>
<head>
	<title>Responsi Basis Data</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <style>
		a:link, a:visited {
  background-color: #f44336;
  color: white;
  padding: 5px 3px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a:hover, a:active {
  background-color: red;

}
th{
	padding: 15px 3px;
	background-color: #f44336;
	  
	text-align: center;
  text-decoration: none;
 
}
	</style>
</head>
<body>
<center>
	<div class="judul">		
		<h1>Bolux Jogja</h1>
		<h2>Data Penjualan Bolux Jogja</h2>
	</div>
	<br/>
 
	<br/>
	
 
	<table border="1" class="table">
		<tr>
			
            <th>Id_Penjualan</th>
			<th>Id_bolu</th>
			<th>Tanggal</th>
			<th>Jumlah</th>
			<th>Harga</th>	
            <th>Total Harga</th>
            <th>Laba</th>		
		</tr>
        
        <?php
            foreach($resultss as $resultt) :
        ?>
		<tr>

			<td><?php echo $resultt['id_penj']; ?></td>
            <td><?php echo $resultt['id_bolu']; ?></td>
			<td><?php echo $resultt['tanggal']; ?></td>
			<td><?php echo $resultt['jumlah']; ?></td>
			<td><?php echo $resultt['harga']; ?></td>
            <td><?php echo $resultt['total_harga']; ?></td>
            <td><?php echo $resultt['laba']; ?></td>

		</tr>
		
        <?php
            endforeach;
        ?>
	</table>
    <br><br><a class="tombol" href="in_penj.php">+ Tambah Data Penjualan Baru</a>
    <br><br><a class="tombol" href="index.php">+ Lihat Data Bolu</a>
    <br><br><a class="tombol" href="lihat_penjualan.php">+ Lihat Pendapatan</a>;
    </center>
</body>
</html>
